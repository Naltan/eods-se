class DefaultEventhandlers;
class CfgPatches
{
	class EODS_RG31
	{
		units[]=
		{
			"ExA_RG31_M2",
			"ExA_RG31_M2_OD",
			"ExA_RG31_Mk19",
			"ExA_RG31_Mk19_OD",
			"ExA_RG31_M2_GC",
			"ExA_RG31E_M2"
		};
		weapons[]=
		{
			"RGM19safe""RGM2safe",
			"RGGMG_40mm"
		};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"A3_Soft_F"
		};
	};
};
class Extended_Init_EventHandlers
{
	class ExA_RG31_BASE
	{
		ExA_RG31_BASE_init="[_this select 0] execVM '\EODS_RG31\scripts\rg31_init.sqf'";
	};
};
class CfgMovesBasic
{
	class DefaultDie;
	class ManActions
	{
		rg31_Driver="rg31_Driver";
		rg31_Gunner="rg31_Gunner";
		rg31_Gunner_Turnin="rg31_Gunner_Turnin";
		Stryker_Cargo01="Stryker_Cargo01";
		rg31_Driver_New="rg31_Driver_New";
		rg31_Commander="rg31_Commander";
		rg31_Driver_New2="rg31_Driver_New2";
	};
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class Crew;
		class rg31_Driver: Crew
		{
			file="\EODS_RG31\Data\Anim\rg31_Driver";
			connectTo[]=
			{
				"rg31_KIA_Driver",
				1
			};
			speed=1e+010;
		};
		class rg31_Commander: Crew
		{
			file="\EODS_RG31\Data\Anim\rg31_Commander";
			connectTo[]=
			{
				"rg31_KIA_Driver",
				1
			};
			speed=1e+010;
		};
		class rg31_Driver_New: Crew
		{
			file="\EODS_RG31\Data\Anim\rg31_Driver_New";
			connectTo[]=
			{
				"rg31_KIA_Driver",
				1
			};
			speed=1e+010;
		};
		class rg31_Driver_New2: Crew
		{
			file="\EODS_RG31\Data\Anim\rg31_Driver_New2";
			connectTo[]=
			{
				"rg31_KIA_Driver",
				1
			};
			speed=1e+010;
		};
		class rg31_KIA_Driver: DefaultDie
		{
			actions="DeadActions";
			file="\EODS_RG31\Data\Anim\rg31_KIA_Driver";
			speed=1e+010;
			terminal=1;
			soundEnabled=0;
			looped=0;
			connectTo[]=
			{
				"DeadState",
				0.1
			};
		};
		class rg31_Gunner: Crew
		{
			file="\EODS_RG31\Data\Anim\rg31_Gunner";
			connectTo[]=
			{
				"rg31_KIA_Gunner",
				1
			};
			speed=1e+010;
		};
		class rg31_KIA_Gunner: DefaultDie
		{
			actions="DeadActions";
			file="\EODS_RG31\Data\Anim\rg31_KIA_Gunner";
			speed=1e+010;
			terminal=1;
			soundEnabled=0;
			looped=0;
			connectTo[]=
			{
				"DeadState",
				0.1
			};
		};
		class rg31_Gunner_Turnin: Crew
		{
			file="\EODS_RG31\Data\Anim\rg31_Gunner_turnin";
			connectTo[]=
			{
				"rg31_KIA_Gunner",
				1
			};
			speed=1e+010;
		};
		class Stryker_Cargo01: Crew
		{
			file="\EODS_RG31\Data\Anim\Stryker\Stryker_Cargo01_V0";
			connectTo[]=
			{
				"KIA_Stryker_Cargo01",
				1
			};
			speed=1e+010;
		};
		class KIA_Stryker_Cargo01: DefaultDie
		{
			actions="DeadActions";
			file="\EODS_RG31\Data\Anim\Stryker\Stryker_KIA_Driver";
			speed=1e+010;
			terminal=1;
			soundEnabled=0;
			looped=0;
			connectTo[]=
			{
				"DeadState",
				0.1
			};
		};
	};
};
class WeaponFireGun;
class WeaponCloudsGun;
class WeaponFireMGun;
class WeaponCloudsMGun;
class cfgWeapons
{
	class Default;
	class RocketPods;
	class HMG_M2;
	class GMG_40mm;
	class RGGMG_40mm: GMG_40mm
	{
		ballisticsComputer=2;
	};
	class RGm2safe: RocketPods
	{
		CanLock=0;
		displayName="SAFETY ON";
		displayNameMagazine="SAFETY ON";
		shortNameMagazine="SAFETY ON";
		nameSound="";
		cursor="EmptyCursor";
		cursorAim="EmptyCursor";
		magazines[]={};
		burst=0;
		reloadTime=0.0099999998;
		magazineReloadTime=0.1;
	};
	class RGm19safe: RocketPods
	{
		CanLock=0;
		displayName="SAFETY ON";
		displayNameMagazine="SAFETY ON";
		shortNameMagazine="SAFETY ON";
		nameSound="";
		cursor="EmptyCursor";
		cursorAim="EmptyCursor";
		magazines[]={};
		burst=0;
		reloadTime=0.0099999998;
		magazineReloadTime=0.1;
	};
};
class CfgVehicles
{
	class Logic;
	class exa_satcom: Logic
	{
		displayName="(ExA) SatCom";
		vehicleClass="Modules";
		icon="\EODS_RG31\data\ico\icon_satcom_ca.paa";
		picture="\EODS_RG31\data\ico\icon_satcom_ca.paa";
		class Eventhandlers
		{
			init="if (isServer) then {private [""_ok""];_ok = _this execVM ""EODS_RG31\modules\satcom\satcom.sqf""};";
		};
	};
	class exa_crewduke: Logic
	{
		displayName="(ExA) CREW Duke";
		vehicleClass="Modules";
		icon="\EODS_RG31\data\ico\icon_crewduke_ca.paa";
		picture="\EODS_RG31\data\ico\icon_crewduke_ca.paa";
		class Eventhandlers
		{
			init="if (isServer) then {private [""_ok""];_ok = _this execVM ""EODS_RG31\modules\crewduke\crewduke.sqf""};";
		};
	};
	class EODS_Towing_Module: Logic
	{
		displayName="EODS Towing Module";
		vehicleClass="Modules";
		icon="\EODS_RG31\data\ico\icon_crewduke_ca.paa";
		picture="\EODS_RG31\data\ico\icon_crewduke_ca.paa";
		class Eventhandlers
		{
			init="if (isServer) then {private [""_ok""];_ok = _this execVM ""EODS_RG31\modules\Towing\EODS_Towing_Module.sqf""};";
		};
	};
	class EODS_Rhino_Module: Logic
	{
		displayName="EODS Rhino Module";
		vehicleClass="Modules";
		icon="\EODS_RG31\data\ico\icon_crewduke_ca.paa";
		picture="\EODS_RG31\data\ico\icon_crewduke_ca.paa";
		class Eventhandlers
		{
			init="if (isServer) then {private [""_ok""];_ok = _this execVM ""EODS_RG31\modules\Rhino\Rhino_hide.sqf""};";
		};
	};
	class exa_boomerang: Logic
	{
		displayName="(ExA) Boomerang";
		vehicleClass="Modules";
		icon="\EODS_RG31\data\ico\icon_boomerang_ca.paa";
		picture="\EODS_RG31\data\ico\icon_boomerang_ca.paa";
		class Eventhandlers
		{
			init="if (isServer) then {private [""_ok""];_ok = _this execVM ""EODS_RG31\modules\boomerang\boomerang.sqf""};";
		};
	};
	class exa_spare: Logic
	{
		displayName="(ExA) Spare wheels (ACE)";
		vehicleClass="Modules";
		icon="\EODS_RG31\data\ico\icon_boomerang_ca.paa";
		picture="\EODS_RG31\data\ico\icon_boomerang_ca.paa";
		class Eventhandlers
		{
			init="if (isServer) then {private [""_ok""];_ok = _this execVM ""EODS_RG31\modules\wheels\ace.sqf""};";
		};
	};
	class LandVehicle;
	class Car: LandVehicle
	{
		class HitPoints;
		class NewTurret;
	};
	class Car_F: Car
	{
		class Turrets
		{
			class MainTurret: NewTurret
			{
				disableSoundAttenuation=1;
				class ViewOptics;
			};
		};
		class HitPoints
		{
			class HitLFWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRF2Wheel;
			class HitBody;
			class HitGlass1
			{
			};
			class HitGlass2;
		};
		class EventHandlers;
		class AnimationSources;
	};
	class ExA_RG31_BASE: Car_F
	{
		htMin=60;
		htMax=1800;
		afMax=200;
		mfMax=100;
		mFact=1;
		tBody=250;
		features="Randomization: No					<br />Firing from vehicles: yes						<br />Slingload: Slingloadable";
		scope=0;
		displayName="RG-31 M2 (ExA)";
		displayNameShort="RG-31 (M2)";
		enableGPS=1;
		model="\EODS_RG31\ExA_RG31_50";
		Picture="\EODS_RG31\data\ico\RG50_Pic_ca.paa";
		icon="\EODS_RG31\data\ico\icomap_RG31_m2_ca.paa";
		hiddenSelections[]=
		{
			"ID_branch",
			"ID_letter",
			"ID_number",
			"ID_serial",
			"ID_custom"
		};
		hiddenSelectionsTextures[]=
		{
			"\EODS_RG31\data\id\id_usmc_ca.paa",
			"\EODS_RG31\data\id\id_a_ca.paa",
			"\EODS_RG31\data\id\id_1_ca.paa",
			"",
			""
		};
		mapSize=6;
		tf_RadioType_api="tf_rt1523g";
		tf_hasLRradio_api=1;
		tf_isolatedAmount_api=0.5;
		side=1;
		faction="Blu_f";
		vehicleClass="Car";
		crew="B_Soldier_f";
		typicalCargo[]=
		{
			"B_Soldier_f",
			"B_Soldier_f",
			"B_soldier_AT_F",
			"B_officer_F"
		};
		cargoAction[]=
		{
			"Stryker_Cargo01"
		};
		transportSoldier=6;
		transportMaxBackpacks=6;
		transportMaxMagazines=999;
		transportMaxWeapons=50;
		cargoIsCoDriver[]={1,0,0,0,0};
		armor=400;
		damageResistance=0.050000001;
		crewCrashProtection=0.80000001;
		crewExplosionProtection=0.99900001;
		armorStructural=8;
		weapons[]=
		{
			"CarHorn"
		};
		damperSize=0.2;
		damperForce=1;
		damperDamping=1;
		WaterLeakiness=10;
		maxFordingDepth=0.5;
		waterResistance=1;
		turnCoef=2.4000001;
		antiRollbarForceCoef=100;
		antiRollbarForceLimit=15;
		antiRollbarSpeedMin=10;
		antiRollbarSpeedMax=100;
		steerAheadSimul=0.2;
		steerAheadPlan=0.30000001;
		predictTurnSimul=0.89999998;
		predictTurnPlan=1;
		brakeDistance=1;
		terrainCoef=1.5;
		wheelCircumference=2.1159999;
		outsideSoundFilter=1;
		insideSoundCoef=0.80000001;
		memoryPointsGetInDriver="pos driver";
		memoryPointsGetInDriverDir="pos driver dir";
		memoryPointsGetInCommander="pos cargo";
		memoryPointsGetInCommanderDir="pos cargo dir";
		memoryPointsGetInCargo="pos cargo";
		memoryPointsGetInCargoDir="pos cargo dir";
		memoryPointsGetInCoDriver="pos codriver";
		memoryPointsGetInCoDriverDir="pos codriver dir";
		memoryPointSupply="supply";
		memoryPointExhaust="exhaust_start";
		memoryPointExhaustDir="exhaust_end";
		class TransportMagazines
		{
		};
		class TransportItems
		{
		};
		class TransportWeapons
		{
		};
		selectionFireAnim="zasleh";
		fuelCapacity=246;
		crewVulnerable=1;
		maxSpeed=100;
		threat[]={1,0.40000001,0.5};
		slingLoadCargoMemoryPoints[]=
		{
			"SlingLoadCargo1",
			"SlingLoadCargo2",
			"SlingLoadCargo3",
			"SlingLoadCargo4"
		};
		viewCargoShadow=1;
		viewCargoShadowDiff=0.050000001;
		viewDriverShadowDiff=0.050000001;
		viewGunnerShadowDiff=0.050000001;
		LODTurnedOut=1100;
		driverIsCommander=0;
		driverAction="rg31_Driver_New2";
		driverInAction="rg31_Driver_New2";
		enableManualFire=0;
		driverCompartments="Compartment1";
		cargoCompartments[]=
		{
			"Compartment1"
		};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				body="mainTurret";
				gun="mainGun";
				hasGunner=1;
				weapons[]=
				{
					"RGm2safe",
					"HMG_M2"
				};
				magazines[]=
				{
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red"
				};
				soundServo[]=
				{
					"\EODS_RG31\data\sound\turret",
					1,
					1,
					70
				};
				minElev=-15;
				maxElev=30;
				gunnerAction="rg31_Gunner";
				viewGunnerInExternal="true";
				castGunnerShadow=1;
				stabilizedInAxes="StabilizedInAxesboth";
				gunnerRightHandAnimName="OtocHlaven_shake";
				gunnerLeftHandAnimName="OtocHlaven_shake";
				memoryPointGunnerOptics="MainGunnerview";
				class ViewOptics
				{
					initAngleX=0;
					minAngleX=-30;
					maxAngleX=30;
					initAngleY=0;
					minAngleY=-100;
					maxAngleY=100;
					initFov=0.69999999;
					minFov=0.25;
					maxFov=1.1;
				};
				class ViewGunner: ViewOptics
				{
				};
			};
		};
		class Reflectors
		{
			class Left
			{
				color[]={1900,1800,1700};
				ambient[]={5,5,5};
				position="light_left";
				direction="light_left_end";
				hitpoint="light_left";
				selection="light_left";
				size=1;
				innerAngle=100;
				outerAngle=179;
				coneFadeCoef=10;
				intensity=1;
				useFlare=1;
				dayLight=1;
				flareSize=1.5;
				class Attenuation
				{
					start=1;
					constant=0;
					linear=0;
					quadratic=0.25;
					hardLimitStart=30;
					hardLimitEnd=60;
				};
			};
			class Left2: Left
			{
				position="light_left2";
				direction="light_left2_end";
				hitpoint="light_left2";
				selection="light_left2";
			};
			class Left3: Left
			{
				position="light_left3";
				direction="light_left3_end";
				hitpoint="light_left3";
				selection="light_left3";
			};
			class Right: Left
			{
				position="light_right";
				direction="light_right_end";
				hitpoint="light_right";
				selection="light_right";
			};
			class Right2: Left
			{
				position="light_right2";
				direction="light_right2_end";
				hitpoint="light_right2";
				selection="light_right2";
			};
			class Right3: Left
			{
				position="light_right3";
				direction="light_right3_end";
				hitpoint="light_right3";
				selection="light_right3";
			};
		};
		class CargoLight
		{
			ambient[]={5,5,5};
			brightness=1;
			color[]={0,0,0,0};
		};
		aggregateReflectors[]=
		{
			
			{
				"Left",
				"Left2"
			},
			
			{
				"Right",
				"Right2"
			},
			
			{
				"Left3",
				"Right3"
			}
		};
		class HitPoints: HitPoints
		{
			class HitGlass1
			{
				armor=2;
				material=-1;
				name="glass1";
				visual="glass1";
				passThrough=1;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitGlass2: HitGlass1
			{
				name="glass2";
				visual="glass2";
			};
			class HitGlass3: HitGlass1
			{
				name="glass3";
				visual="glass3";
			};
			class HitGlass4: HitGlass1
			{
				name="glass4";
				visual="glass4";
			};
			class HitGlass5: HitGlass1
			{
				name="glass5";
				visual="glass5";
			};
			class HitGlass6: HitGlass1
			{
				name="glass6";
				visual="glass6";
			};
			class HitGlass7: HitGlass1
			{
				name="glass7";
				visual="glass7";
			};
			class HitGlass8: HitGlass1
			{
				name="glass8";
				visual="glass8";
			};
			class HitGlass9: HitGlass1
			{
				name="glass9";
				visual="glass9";
			};
			class HitGlass10: HitGlass1
			{
				name="glass10";
				visual="glass10";
			};
			class HitGlass11: HitGlass1
			{
				name="glass11";
				visual="glass11";
			};
			class HitGlass12: HitGlass1
			{
				name="glass12";
				visual="glass12";
			};
			class HitGlass13: HitGlass1
			{
				name="glass13";
				visual="glass13";
			};
			class HitGlass14: HitGlass1
			{
				name="glass14";
				visual="glass14";
			};
			class HitGlass15: HitGlass1
			{
				name="glass15";
				visual="glass15";
			};
			class HitGlass16: HitGlass1
			{
				name="glass16";
				visual="glass16";
			};
			class HitHull
			{
				armor=1;
				material=-1;
				name="karoserie";
				visual="";
				passThrough=1;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitBottomArmor
			{
				armor=3;
				material=-1;
				name="Armor_Bottom";
				visual="";
				passThrough=1;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitLeftArmor
			{
				armor=2;
				material=-1;
				name="Armor_Left";
				visual="";
				passThrough=0.80000001;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitRightArmor
			{
				armor=2;
				material=-1;
				name="Armor_Right";
				visual="";
				passThrough=0.80000001;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitTopArmor
			{
				armor=3;
				material=-1;
				name="Armor_Top";
				visual="";
				passThrough=0.5;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitFuel
			{
				armor=1;
				material=-1;
				name="palivo";
				visual="";
				passThrough=1;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitLFWheel
			{
				armor=0.2;
				material=-1;
				name="wheel_1_1_steering";
				visual="";
				passThrough=0.30000001;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitLBWheel
			{
				armor=0.2;
				material=-1;
				name="wheel_1_2_steering";
				visual="";
				passThrough=0.30000001;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitRFWheel
			{
				armor=0.2;
				material=-1;
				name="wheel_2_1_steering";
				visual="";
				passThrough=0.30000001;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitRBWheel
			{
				armor=0.2;
				material=-1;
				name="wheel_2_2_steering";
				visual="";
				passThrough=0.30000001;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
			class HitEngine
			{
				armor=1;
				material=-1;
				name="motor";
				visual="";
				passThrough=0.2;
				minimalHit=0.1;
				explosionShielding=0.5;
				radius=0.15000001;
			};
		};
		class AnimationSources: AnimationSources
		{
			class HitLFWheel
			{
				source="Hit";
				hitpoint="HitLFWheel";
				raw=1;
			};
			class HitRFWheel: HitLFWheel
			{
				hitpoint="HitRFWheel";
			};
			class HitLBWheel: HitLFWheel
			{
				hitpoint="HitLBWheel";
			};
			class HitRBWheel: HitLFWheel
			{
				hitpoint="HitRBWheel";
			};
			class HitGlass1
			{
				source="Hit";
				hitpoint="HitGlass1";
				raw=1;
			};
			class HitGlass2: HitGlass1
			{
				hitpoint="HitGlass2";
			};
			class HitGlass3: HitGlass1
			{
				hitpoint="HitGlass3";
			};
			class HitGlass4: HitGlass1
			{
				hitpoint="HitGlass4";
			};
			class HitGlass5: HitGlass1
			{
				hitpoint="HitGlass5";
			};
			class HitGlass6: HitGlass1
			{
				hitpoint="HitGlass6";
			};
			class HitGlass7: HitGlass1
			{
				hitpoint="HitGlass7";
			};
			class HitGlass8: HitGlass1
			{
				hitpoint="HitGlass8";
			};
			class HitGlass9: HitGlass1
			{
				hitpoint="HitGlass9";
			};
			class HitGlass10: HitGlass1
			{
				hitpoint="HitGlass10";
			};
			class HitGlass11: HitGlass1
			{
				hitpoint="HitGlass11";
			};
			class HitGlass12: HitGlass1
			{
				hitpoint="HitGlass12";
			};
			class HitGlass13: HitGlass1
			{
				hitpoint="HitGlass13";
			};
			class HitGlass14: HitGlass1
			{
				hitpoint="HitGlass14";
			};
			class HitGlass15: HitGlass1
			{
				hitpoint="HitGlass15";
			};
			class HitGlass16: HitGlass1
			{
				hitpoint="HitGlass16";
			};
			class vs17
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class Towing_Module
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class Rhino_hide
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class camonet
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class SYS_crewduke
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class SYS_boomerang
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class SYS_satcom
			{
				source="user";
				animPeriod=0.1;
				initPhase=1;
			};
			class left_spare
			{
				source="user";
				animPeriod=0.1;
				initPhase=0;
			};
			class right_spare
			{
				source="user";
				animPeriod=0.1;
				initPhase=0;
			};
			class antenna
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class Open_door_rear
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class Open_door_Left
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class Open_door_Right
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class Rhino_Anim
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class BeaconsStart
			{
				source="user";
				animPeriod=1;
				initPhase=1;
			};
		};
		class UserActions
		{
			class beacons_start
			{
				userActionID=50;
				displayName="Beacons start";
				position="mph_axis";
				radius=1.8;
				onlyForplayer="false";
				condition="this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",0];";
			};
			class beacons_stop: beacons_start
			{
				userActionID=51;
				displayName="Beacons stop";
				condition="this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",1];";
			};
			class Open_door_rear
			{
				displayName="Open_door_rear";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Open_door_rear"" < 0.5";
				statement="this animate [""Open_door_rear"", 1]";
			};
			class Close_door_rear: Open_door_rear
			{
				displayName="Close_door_rear";
				condition="this animationPhase ""Open_door_rear"" >= 0.5";
				statement="this animate [""Open_door_rear"", 0]";
			};
			class Rhino_Anim
			{
				displayName="Activate Rhino";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Rhino_Anim"" < 0.5";
				statement="this animate [""Rhino_Anim"", 1]";
			};
			class Rhino_Anim_Revert: Rhino_Anim
			{
				displayName="Desactivate Rhino";
				condition="this animationPhase ""Rhino_Anim"" >= 0.5";
				statement="this animate [""Rhino_Anim"", 0]";
			};
		};
		attenuationEffectType="CarAttenuation";
		soundGetIn[]=
		{
			"A3\Sounds_F\vehicles\soft\MRAP_01\getin",
			0.56234133,
			1
		};
		soundGetOut[]=
		{
			"A3\Sounds_F\vehicles\soft\MRAP_01\getout",
			0.56234133,
			1,
			40
		};
		soundDammage[]=
		{
			"",
			0.56234133,
			1
		};
		soundEngineOnInt[]=
		{
			"\EODS_RG31\data\sound\INT_On",
			0.56234097,
			1
		};
		soundEngineOnExt[]=
		{
			"\EODS_RG31\data\sound\EXT_On",
			0.56234097,
			1,
			350
		};
		soundEngineOffInt[]=
		{
			"\EODS_RG31\data\sound\INT_Off",
			0.56234097,
			1
		};
		soundEngineOffExt[]=
		{
			"\EODS_RG31\data\sound\EXT_Off",
			0.56234097,
			1,
			350
		};
		buildCrash0[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_building_01",
			1,
			1,
			200
		};
		buildCrash1[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_building_02",
			1,
			1,
			200
		};
		buildCrash2[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_building_03",
			1,
			1,
			200
		};
		buildCrash3[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_building_04",
			1,
			1,
			200
		};
		soundBuildingCrash[]=
		{
			"buildCrash0",
			0.25,
			"buildCrash1",
			0.25,
			"buildCrash2",
			0.25,
			"buildCrash3",
			0.25
		};
		WoodCrash0[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_mix_wood_01",
			1,
			1,
			200
		};
		WoodCrash1[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_mix_wood_02",
			1,
			1,
			200
		};
		WoodCrash2[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_mix_wood_03",
			1,
			1,
			200
		};
		WoodCrash3[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_mix_wood_04",
			1,
			1,
			200
		};
		WoodCrash4[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_mix_wood_05",
			1,
			1,
			200
		};
		WoodCrash5[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_mix_wood_06",
			1,
			1,
			200
		};
		soundWoodCrash[]=
		{
			"woodCrash0",
			0.16599999,
			"woodCrash1",
			0.16599999,
			"woodCrash2",
			0.16599999,
			"woodCrash3",
			0.16599999,
			"woodCrash4",
			0.16599999,
			"woodCrash5",
			0.16599999
		};
		ArmorCrash0[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_vehicle_01",
			1,
			1,
			200
		};
		ArmorCrash1[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_vehicle_02",
			1,
			1,
			200
		};
		ArmorCrash2[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_vehicle_03",
			1,
			1,
			200
		};
		ArmorCrash3[]=
		{
			"A3\sounds_f\Vehicles\soft\noises\crash_vehicle_04",
			1,
			1,
			200
		};
		soundArmorCrash[]=
		{
			"ArmorCrash0",
			0.25,
			"ArmorCrash1",
			0.25,
			"ArmorCrash2",
			0.25,
			"ArmorCrash3",
			0.25
		};
		class Sounds
		{
			class Idle_ext
			{
				sound[]=
				{
					"\EODS_RG31\data\sound\EXT_Idle",
					0.39810717,
					1,
					150
				};
				frequency="0.95	+	((rpm/	4500) factor[(800/	4500),(1400/	4500)])*0.15";
				volume="engineOn*camPos*(((rpm/	4500) factor[(600/	4500),(1100/	4500)])	*	((rpm/	4500) factor[(1800/	4500),(1300/	4500)]))";
			};
			class Engine
			{
				sound[]=
				{
					"\EODS_RG31\data\sound\EXT_Low",
					0.44668359,
					1,
					250
				};
				frequency="0.9	+	((rpm/	4500) factor[(1400/	4500),(2100/	4500)])*0.2";
				volume="engineOn*camPos*(((rpm/	4500) factor[(1400/	4500),(1800/	4500)])	*	((rpm/	4500) factor[(2300/	4500),(2000/	4500)]))";
			};
			class Engine1_ext
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_low2",
					0.56234133,
					1,
					300
				};
				frequency="0.9	+		((rpm/	4500) factor[(2100/	4500),(2800/	4500)])*0.2";
				volume="engineOn*camPos*(((rpm/	4500) factor[(1900/	4500),(2300/	4500)])	*	((rpm/	4500) factor[(3000/	4500),(2500/	4500)]))";
			};
			class Engine2_ext
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_mid",
					0.70794576,
					1,
					350
				};
				frequency="0.9	+	((rpm/	4500) factor[(2800/	4500),(3600/	4500)])*0.2";
				volume="engineOn*camPos*(((rpm/	4500) factor[(2500/	4500),(3100/	4500)])	*	((rpm/	4500) factor[(4500/	4500),(3700/	4500)]))";
			};
			class Engine3_ext
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_high",
					1,
					1,
					400
				};
				frequency="0.95	+	((rpm/	4500) factor[(3600/	4500),(4500/	4500)])*0.1";
				volume="engineOn*camPos*((rpm/	4500) factor[(3800/	4500),(4500/	4500)])";
			};
			class IdleThrust
			{
				sound[]=
				{
					"\EODS_RG31\data\sound\INT_Idle",
					0.56234133,
					1,
					200
				};
				frequency="0.95	+	((rpm/	4500) factor[(800/	4500),(1400/	4500)])*0.15";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(600/	4500),(1100/	4500)])	*	((rpm/	4500) factor[(1800/	4500),(1300/	4500)]))";
			};
			class EngineThrust
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_exhaust_low1",
					0.70794576,
					1,
					350
				};
				frequency="0.9	+	((rpm/	4500) factor[(1400/	4500),(2100/	4500)])*0.2";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(1400/	4500),(1800/	4500)])	*	((rpm/	4500) factor[(2300/	4500),(2000/	4500)]))";
			};
			class Engine1_Thrust_ext
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_exhaust_low2",
					0.89125091,
					1,
					400
				};
				frequency="0.9	+		((rpm/	4500) factor[(2100/	4500),(2800/	4500)])*0.2";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(1900/	4500),(2300/	4500)])	*	((rpm/	4500) factor[(3000/	4500),(2500/	4500)]))";
			};
			class Engine2_Thrust_ext
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_exhaust_mid",
					1.1220185,
					1,
					425
				};
				frequency="0.9	+	((rpm/	4500) factor[(2800/	4500),(3600/	4500)])*0.2";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(2500/	4500),(3100/	4500)])	*	((rpm/	4500) factor[(4500/	4500),(3700/	4500)]))";
			};
			class Engine3_Thrust_ext
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_ext_exhaust_high",
					1.2589254,
					1,
					450
				};
				frequency="0.95	+	((rpm/	4500) factor[(3600/	4500),(4500/	4500)])*0.1";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/	4500) factor[(3800/	4500),(4500/	4500)])";
			};
			class Idle_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_idle",
					0.25118864,
					1
				};
				frequency="0.95	+	((rpm/	4500) factor[(800/	4500),(1400/	4500)])*0.15";
				volume="engineOn*(1-camPos)*(((rpm/	4500) factor[(600/	4500),(1100/	4500)])	*	((rpm/	4500) factor[(1800/	4500),(1300/	4500)]))";
			};
			class Engine_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_low1",
					0.31622776,
					1
				};
				frequency="0.9	+	((rpm/	4500) factor[(1400/	4500),(2100/	4500)])*0.2";
				volume="engineOn*(1-camPos)*(((rpm/	4500) factor[(1400/	4500),(1800/	4500)])	*	((rpm/	4500) factor[(2300/	4500),(2000/	4500)]))";
			};
			class Engine1_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_low2",
					0.39810717,
					1
				};
				frequency="0.9	+		((rpm/	4500) factor[(2100/	4500),(2800/	4500)])*0.2";
				volume="engineOn*(1-camPos)*(((rpm/	4500) factor[(1900/	4500),(2300/	4500)])	*	((rpm/	4500) factor[(3000/	4500),(2500/	4500)]))";
			};
			class Engine2_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_mid",
					0.50118721,
					1
				};
				frequency="0.9	+	((rpm/	4500) factor[(2800/	4500),(3600/	4500)])*0.2";
				volume="engineOn*(1-camPos)*(((rpm/	4500) factor[(2500/	4500),(3100/	4500)])	*	((rpm/	4500) factor[(4500/	4500),(3700/	4500)]))";
			};
			class Engine3_int
			{
				sound[]=
				{
					"\EODS_RG31\data\sound\INT_High",
					0.63095737,
					1
				};
				frequency="0.95	+	((rpm/	4500) factor[(3600/	4500),(4500/	4500)])*0.1";
				volume="engineOn*(1-camPos)*((rpm/	4500) factor[(3800/	4500),(4500/	4500)])";
			};
			class IdleThrust_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_exhaust_idle",
					0.35481337,
					1
				};
				frequency="0.95	+	((rpm/	4500) factor[(800/	4500),(1400/	4500)])*0.15";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(600/	4500),(1100/	4500)])	*	((rpm/	4500) factor[(1800/	4500),(1300/	4500)]))";
			};
			class EngineThrust_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_exhaust_low1",
					0.44668359,
					1
				};
				frequency="0.9	+	((rpm/	4500) factor[(1400/	4500),(2100/	4500)])*0.2";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(1400/	4500),(1800/	4500)])	*	((rpm/	4500) factor[(2300/	4500),(2000/	4500)]))";
			};
			class Engine1_Thrust_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_exhaust_low2",
					0.56234133,
					1
				};
				frequency="0.9	+		((rpm/	4500) factor[(2100/	4500),(2800/	4500)])*0.2";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(1900/	4500),(2300/	4500)])	*	((rpm/	4500) factor[(3000/	4500),(2500/	4500)]))";
			};
			class Engine2_Thrust_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_exhaust_mid",
					0.70794576,
					1
				};
				frequency="0.9	+	((rpm/	4500) factor[(2800/	4500),(3600/	4500)])*0.2";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/	4500) factor[(2500/	4500),(3100/	4500)])	*	((rpm/	4500) factor[(4500/	4500),(3700/	4500)]))";
			};
			class Engine3_Thrust_int
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\MRAP_01\MRAP_01_int_exhaust_high",
					0.79432821,
					1
				};
				frequency="0.95	+	((rpm/	4500) factor[(3600/	4500),(4500/	4500)])*0.1";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/	4500) factor[(3800/	4500),(4500/	4500)])";
			};
			class TiresRockOut
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_1",
					1.4125376,
					1,
					60
				};
				frequency="1";
				volume="camPos*rock*(speed factor[2, 20])";
			};
			class TiresSandOut
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\ext-tires-sand1",
					1.4125376,
					1,
					60
				};
				frequency="1";
				volume="camPos*sand*(speed factor[2, 20])";
			};
			class TiresGrassOut
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_2",
					1.2589254,
					1,
					60
				};
				frequency="1";
				volume="camPos*grass*(speed factor[2, 20])";
			};
			class TiresMudOut
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\ext-tires-mud2",
					1.1220185,
					1,
					60
				};
				frequency="1";
				volume="camPos*mud*(speed factor[2, 20])";
			};
			class TiresGravelOut
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\ext_tires_gravel_1",
					1.2589254,
					1,
					60
				};
				frequency="1";
				volume="camPos*gravel*(speed factor[2, 20])";
			};
			class TiresAsphaltOut
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\ext_tires_asfalt_2",
					1.1220185,
					1,
					60
				};
				frequency="1";
				volume="camPos*asphalt*(speed factor[2, 20])";
			};
			class NoiseOut
			{
				sound[]=
				{
					"\EODS_RG31\data\sound\EXT_noises",
					1.1220185,
					1,
					90
				};
				frequency="1";
				volume="camPos*(damper0 max 0.02)*(speed factor[0, 8])";
			};
			class TiresRockIn
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_1",
					0.70794576,
					1
				};
				frequency="1";
				volume="(1-camPos)*rock*(speed factor[2, 20])";
			};
			class TiresSandIn
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\int-tires-sand2",
					0.70794576,
					1
				};
				frequency="1";
				volume="(1-camPos)*sand*(speed factor[2, 20])";
			};
			class TiresGrassIn
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_2",
					0.70794576,
					1
				};
				frequency="1";
				volume="(1-camPos)*grass*(speed factor[2, 20])";
			};
			class TiresMudIn
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\int-tires-mud2",
					0.70794576,
					1
				};
				frequency="1";
				volume="(1-camPos)*mud*(speed factor[2, 20])";
			};
			class TiresGravelIn
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\int_tires_gravel_1",
					0.70794576,
					1
				};
				frequency="1";
				volume="(1-camPos)*gravel*(speed factor[2, 20])";
			};
			class TiresAsphaltIn
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\tires\int_tires_asfalt_2",
					0.70794576,
					1
				};
				frequency="1";
				volume="(1-camPos)*asphalt*(speed factor[2, 20])";
			};
			class NoiseIn
			{
				sound[]=
				{
					"\EODS_RG31\data\sound\INT_noises",
					0.56234133,
					1
				};
				frequency="1";
				volume="(damper0 max 0.1)*(speed factor[0, 8])*(1-camPos)";
			};
			class breaking_ext_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04",
					0.70794576,
					1,
					80
				};
				frequency=1;
				volume="engineOn*camPos*asphalt*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 10])";
			};
			class acceleration_ext_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02",
					0.70794576,
					1,
					80
				};
				frequency=1;
				volume="engineOn*camPos*asphalt*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
			};
			class turn_left_ext_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02",
					0.70794576,
					1,
					80
				};
				frequency=1;
				volume="engineOn*camPos*asphalt*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
			};
			class turn_right_ext_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02",
					0.70794576,
					1,
					80
				};
				frequency=1;
				volume="engineOn*camPos*asphalt*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
			};
			class breaking_ext_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking",
					0.70794576,
					1,
					60
				};
				frequency=1;
				volume="engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 10])";
			};
			class acceleration_ext_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\acceleration_dirt_ext_1",
					0.70794576,
					1,
					60
				};
				frequency=1;
				volume="engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
			};
			class turn_left_ext_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt",
					0.70794576,
					1,
					60
				};
				frequency=1;
				volume="engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
			};
			class turn_right_ext_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt",
					0.70794576,
					1,
					60
				};
				frequency=1;
				volume="engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
			};
			class breaking_int_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 6])";
			};
			class acceleration_int_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
			};
			class turn_left_int_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
			};
			class turn_right_int_road
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
			};
			class breaking_int_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[-0.15, -0.3])*(Speed Factor[2, 6])";
			};
			class acceleration_int_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\acceleration_dirt_int_1",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[0.15, 0.3])*(Speed Factor[10, 0])";
			};
			class turn_left_int_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[0.15, 0.3])*(Speed Factor[0, 10])";
			};
			class turn_right_int_dirt
			{
				sound[]=
				{
					"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int",
					0.31622776,
					1
				};
				frequency=1;
				volume="engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[-0.15, -0.3])*(Speed Factor[0, 10])";
			};
		};
		driverLeftHandAnimName="drivewheel";
		driverRightHandAnimName="";
		driverLeftLegAnimName="";
		driverRightLegAnimName="pedal_thrust";
		idleRpm=800;
		redRpm=4500;
		brakeIdleSpeed=1;
		class complexGearbox
		{
			GearboxRatios[]=
			{
				"R1",
				-5.75,
				"N",
				0,
				"D1",
				4.3000002,
				"D2",
				2.3,
				"D3",
				1.5,
				"D4",
				1,
				"D5",
				0.73000002
			};
			TransmissionRatios[]=
			{
				"High",
				6.7589998
			};
			gearBoxMode="auto";
			moveOffGear=1;
			driveString="D";
			neutralString="N";
			reverseString="R";
		};
		simulation="carx";
		dampersBumpCoef=6;
		differentialType="all_limited";
		frontRearSplit=0.5;
		frontBias=1.3;
		rearBias=1.3;
		centreBias=1.3;
		clutchStrength=20;
		enginePower=676;
		maxOmega=720;
		peakTorque=1253;
		dampingRateFullThrottle=0.079999998;
		dampingRateZeroThrottleClutchEngaged=2;
		dampingRateZeroThrottleClutchDisengaged=0.34999999;
		torqueCurve[]=
		{
			{0,0},
			{0.278,0.5},
			{0.34999999,0.75},
			{0.461,1},
			{0.60000002,0.94999999},
			{0.69999999,0.85000002},
			{0.80000001,0.75},
			{1,0.5}
		};
		changeGearMinEffectivity[]={0.94999999,0.15000001,0.94999999,0.94999999,0.94999999,0.94999999,0.94999999};
		switchTime=0.31;
		latency=1;
		class Wheels
		{
			class LF
			{
				boneName="wheel_1_1_damper";
				steering=1;
				side="left";
				center="wheel_1_1_axis";
				boundary="wheel_1_1_bound";
				width="0.126";
				mass=30;
				MOI=10.2;
				dampingRate=0.1;
				maxBrakeTorque=27000;
				maxHandBrakeTorque=0;
				suspTravelDirection[]={0,-1,0};
				suspForceAppPointOffset="wheel_1_1_axis";
				tireForceAppPointOffset="wheel_1_1_axis";
				maxCompression=0.15000001;
				mMaxDroop=0.2;
				sprungMass=2225;
				springStrength=51625;
				springDamperRate=8920;
				longitudinalStiffnessPerUnitGravity=10000;
				latStiffX=25;
				latStiffY=180;
				frictionVsSlipGraph[]=
				{
					{0,1},
					{0.5,1},
					{1,1}
				};
			};
			class LR: LF
			{
				boneName="wheel_1_2_damper";
				steering=0;
				center="wheel_1_2_axis";
				boundary="wheel_1_2_bound";
				suspForceAppPointOffset="wheel_1_2_axis";
				tireForceAppPointOffset="wheel_1_2_axis";
				maxHandBrakeTorque=25000;
			};
			class RF: LF
			{
				boneName="wheel_2_1_damper";
				center="wheel_2_1_axis";
				boundary="wheel_2_1_bound";
				suspForceAppPointOffset="wheel_2_1_axis";
				tireForceAppPointOffset="wheel_2_1_axis";
				steering=1;
				side="right";
			};
			class RR: RF
			{
				boneName="wheel_2_2_damper";
				steering=0;
				center="wheel_2_2_axis";
				boundary="wheel_2_2_bound";
				suspForceAppPointOffset="wheel_2_2_axis";
				tireForceAppPointOffset="wheel_2_2_axis";
				maxHandBrakeTorque=25000;
			};
		};
		class RenderTargets
		{
			class LeftMirror
			{
				renderTarget="rendertarget0";
				class CameraView1
				{
					pointPosition="PIP0_pos";
					pointDirection="PIP0_dir";
					renderQuality=2;
					renderVisionMode=4;
					fov=0.69999999;
				};
			};
			class RightMirror
			{
				renderTarget="rendertarget1";
				class CameraView1
				{
					pointPosition="PIP1_pos";
					pointDirection="PIP1_dir";
					renderQuality=2;
					renderVisionMode=4;
					fov=0.69999999;
				};
			};
			class GyroCamPip
			{
				renderTarget="rendertarget2";
				class CameraView1
				{
					pointPosition="PIP2_pos";
					pointDirection="PIP2_dir";
					renderQuality=2;
					renderVisionMode=4;
					fov=1.5;
				};
			};
		};
	};
	class ExA_RG31_M2: ExA_RG31_BASE
	{
		scope=2;
		model="\EODS_RG31\ExA_RG31_50";
		displayName="RG-31 M2 (ExA)";
		icon="\EODS_RG31\data\ico\icomap_RG31_m2_ca.paa";
		destrType="DestructWreck";
		slingLoadCargoMemoryPoints[]=
		{
			"SlingLoadCargo1",
			"SlingLoadCargo2",
			"SlingLoadCargo3",
			"SlingLoadCargo4"
		};
		transportSoldier=4;
		cargoProxyIndexes[]={1};
		getInProxyOrder[]={1,2,3,4,5,6};
		class CargoTurret;
		class Turrets: Turrets
		{
			class CargoTurret_01: CargoTurret
			{
				gunnerAction="passenger_inside_1";
				gunnerCompartments="Compartment1";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				gunnerName="Rear Right sit";
				proxyIndex=4;
				maxElev=45;
				minElev=-35;
				maxTurn=95;
				minTurn=-95;
				isPersonTurret=1;
				class dynamicViewLimits
				{
					CargoTurret_02[]={-65,95};
				};
			};
			class CargoTurret_02: CargoTurret_01
			{
				gunnerAction="passenger_inside_1";
				gunnerName="Rear Left sit";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				proxyIndex=5;
				class dynamicViewLimits
				{
					CargoTurret_01[]={-95,65};
				};
			};
			class MainTurret: MainTurret
			{
				body="mainTurret";
				gun="mainGun";
				weapons[]=
				{
					"RGm2safe",
					"HMG_M2"
				};
				magazines[]=
				{
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red"
				};
				soundServo[]=
				{
					"\EODS_RG31\data\sound\turret",
					1,
					1,
					70
				};
				minElev=-15;
				maxElev=30;
				gunnerAction="rg31_Gunner";
				viewGunnerInExternal=1;
				castGunnerShadow=1;
				memoryPointGunnerOptics="MainGunnerview";
				stabilizedInAxes="StabilizedInAxesBoth";
				turretInfoType="RscOptics_Offroad_01";
				discreteDistance[]={100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500};
				discreteDistanceInitIndex=2;
				class ViewOptics;
				class ViewGunner: ViewOptics
				{
				};
			};
		};
		showNVGCargo[]={0,1};
		class AnimationSources: AnimationSources
		{
			class ReloadAnim
			{
				source="reload";
				weapon="HMG_M2";
			};
			class ReloadMagazine
			{
				source="reloadmagazine";
				weapon="HMG_M2";
			};
			class Revolving
			{
				source="revolving";
				weapon="HMG_M2";
			};
			class camonet
			{
				source="user";
				animPeriod=0.1;
				initPhase=0;
			};
			class BeaconsStart
			{
				source="user";
				animPeriod=1;
				initPhase=1;
			};
			class Open_door_rear
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
		};
		class UserActions
		{
			class beacons_start
			{
				userActionID=50;
				displayName="Beacons start";
				position="mph_axis";
				radius=1.8;
				onlyForplayer="false";
				condition="this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",0];";
			};
			class beacons_stop: beacons_start
			{
				userActionID=51;
				displayName="Beacons stop";
				condition="this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",1];";
			};
			class Open_door_rear
			{
				displayName="Open_door_rear";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Open_door_rear"" < 0.5";
				statement="this animate [""Open_door_rear"", 1]";
			};
			class Close_door_rear: Open_door_rear
			{
				displayName="Close_door_rear";
				condition="this animationPhase ""Open_door_rear"" >= 0.5";
				statement="this animate [""Open_door_rear"", 0]";
			};
		};
		class Damage
		{
			tex[]={};
			mat[]=
			{
				"EODS_RG31\data\auta_skla.rvmat",
				"EODS_RG31\data\auta_skla_damage.rvmat",
				"EODS_RG31\data\auta_skla_destruct.rvmat",
				"EODS_RG31\data\exa_rg31_body.rvmat",
				"EODS_RG31\data\exa_rg31_body_damage.rvmat",
				"EODS_RG31\data\exa_rg31_body_destruct.rvmat",
				"EODS_RG31\data\exa_rg31_body2.rvmat",
				"EODS_RG31\data\exa_rg31_body2_damage.rvmat",
				"EODS_RG31\data\exa_rg31_body2_destruct.rvmat",
				"EODS_RG31\data\exa_RG31_interior.rvmat",
				"EODS_RG31\data\exa_RG31_interior.rvmat",
				"EODS_RG31\data\exa_RG31_interior_destruct.rvmat",
				"EODS_RG31\data\exa_RG31_interior2.rvmat",
				"EODS_RG31\data\exa_RG31_interior2.rvmat",
				"EODS_RG31\data\exa_RG31_interior2_destruct.rvmat"
			};
		};
		hiddenSelections[]=
		{
			"ID_branch",
			"ID_letter",
			"ID_number",
			"ID_serial",
			"ID_custom"
		};
		hiddenSelectionsTextures[]=
		{
			"\EODS_RG31\data\id\id_usmc_ca.paa",
			"\EODS_RG31\data\id\id_a_ca.paa",
			"\EODS_RG31\data\id\id_1_ca.paa",
			"",
			""
		};
	};
	class ExA_RG31_M2_OD: ExA_RG31_M2
	{
		scope=2;
		model="\EODS_RG31\ExA_RG31_50_OD";
		displayName="RG-31 M2 OD (ExA)";
	};
	class ExA_RG31_M2_GC: ExA_RG31_M2
	{
		scope=2;
		model="\EODS_RG31\ExA_RG31_50_GC";
		displayName="RG-31 M2 GC (ExA)";
		transportSoldier=3;
		class AnimationSources: AnimationSources
		{
			class Gyrocam
			{
				source="User";
				animPeriod=1;
				initPhase=0;
			};
			class Open_door_rear
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class Rhino_Anim
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
			class BeaconsStart
			{
				source="user";
				animPeriod=1;
				initPhase=1;
			};
		};
		class UserActions
		{
			class beacons_start
			{
				userActionID=50;
				displayName="Beacons start";
				position="mph_axis";
				radius=1.8;
				onlyForplayer="false";
				condition="this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",0];";
			};
			class beacons_stop: beacons_start
			{
				userActionID=51;
				displayName="Beacons stop";
				condition="this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",1];";
			};
			class UseCam
			{
				displayName="Use Gyrocam";
				position="pos_action";
				radius=7.5;
				onlyForPlayer="false";
				condition=" (PLAYER == commander this) AND !exa_rg31_gyrocam";
				statement="this execVM ""\EODS_RG31\scripts\gyrocam.sqf"";";
			};
			class Open_door_rear
			{
				displayName="Open_door_rear";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Open_door_rear"" < 0.5";
				statement="this animate [""Open_door_rear"", 1]";
			};
			class Close_door_rear: Open_door_rear
			{
				displayName="Close_door_rear";
				condition="this animationPhase ""Open_door_rear"" >= 0.5";
				statement="this animate [""Open_door_rear"", 0]";
			};
			class Rhino_Anim
			{
				displayName="Activate Rhino";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Rhino_Anim"" < 0.5";
				statement="this animate [""Rhino_Anim"", 1]";
			};
			class Rhino_Anim_Revert: Rhino_Anim
			{
				displayName="Desactivate Rhino";
				condition="this animationPhase ""Rhino_Anim"" >= 0.5";
				statement="this animate [""Rhino_Anim"", 0]";
			};
		};
		class CargoTurret;
		class Turrets: Turrets
		{
			class CargoTurret_01: CargoTurret
			{
				gunnerAction="passenger_inside_1";
				gunnerCompartments="Compartment1";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				gunnerName="Rear Right sit";
				proxyIndex=4;
				maxElev=45;
				minElev=-35;
				maxTurn=95;
				minTurn=-95;
				isPersonTurret=1;
				class dynamicViewLimits
				{
					CargoTurret_02[]={-65,95};
				};
			};
			class CargoTurret_02: CargoTurret_01
			{
				gunnerAction="passenger_inside_1";
				gunnerName="Rear Left sit";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				proxyIndex=5;
				class dynamicViewLimits
				{
					CargoTurret_01[]={-95,65};
				};
			};
			class MainTurret: MainTurret
			{
			};
			class CommanderObs: NewTurret
			{
				stabilizedInAxes="StabilizedInAxesBoth";
				proxytype="CPCommander";
				proxyIndex=1;
				gunnerName="$STR_POSITION_COMMANDER";
				primaryGunner=0;
				primaryObserver=1;
				startEngine=0;
				hasGunner=1;
				gunnerAction="rg31_Commander";
				gunnerInAction="rg31_Commander";
				body="ObsTurret";
				gun="ObsGun";
				animationSourceBody="ObsTurret";
				animationSourceGun="ObsGun";
				outGunnerMayFire=1;
				gunBeg="gun_end";
				gunEnd="gun_begin";
				memoryPointGun="gun_end";
				commanding=1;
				weapons[]={};
				magazines[]={};
				minElev=-50;
				maxElev=85;
				initElev=0;
				minTurn=-180;
				maxTurn=180;
				initTurn=0;
				memoryPointGunnerOptics="commanderview";
				gunnerForceOptics=0;
				gunnerOpticsShowCursor=0;
				gunnerOpticsModel="\A3\weapons_f\reticle\Optics_Commander_02_F";
				turretInfoType="RscOptics_MBT_01_commander";
				class ViewOptics
				{
					initAngleX=0;
					minAngleX=-50;
					maxAngleX=80;
					initAngleY=0;
					minAngleY=-100;
					maxAngleY=100;
					initFov=0.80000001;
					minFov=0.0082999999;
					maxFov=0.80000001;
					turretInfoType="RscOptics_MBT_01_commander";
					visionMode[]=
					{
						"Normal",
						"NVG",
						"Ti"
					};
					thermalMode[]={0,1};
				};
				class ViewGunner
				{
					initAngleX=0;
					minAngleX=-50;
					maxAngleX=80;
					initAngleY=0;
					minAngleY=-100;
					maxAngleY=100;
					initFov=0.80000001;
					minFov=0.0082999999;
					maxFov=0.80000001;
				};
				gunnerCompartments="Compartment1";
				soundServo[]=
				{
					"",
					0.0099999998,
					1
				};
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				selectionFireAnim="";
				castGunnerShadow=1;
				viewGunnerShadow=1;
			};
		};
	};
	class ExA_RG31_Mk19: ExA_RG31_BASE
	{
		scope=2;
		model="\EODS_RG31\ExA_RG31_mk19";
		displayName="RG-31 Mk.19 (ExA)";
		displayNameShort="RG-31 (Mk19)";
		icon="\EODS_RG31\data\ico\icomap_RG31_mk19_ca.paa";
		destrType="DestructWreck";
		class AnimationSources: AnimationSources
		{
			class belt_rotation
			{
				source="reload";
				weapon="GMG_40mm";
			};
			class BeaconsStart
			{
				source="user";
				animPeriod=1;
				initPhase=1;
			};
			class Open_door_rear
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
		};
		class UserActions
		{
			class beacons_start
			{
				userActionID=50;
				displayName="Beacons start";
				position="mph_axis";
				radius=1.8;
				onlyForplayer="false";
				condition="this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",0];";
			};
			class beacons_stop: beacons_start
			{
				userActionID=51;
				displayName="Beacons stop";
				condition="this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",1];";
			};
			class Open_door_rear
			{
				displayName="Open_door_rear";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Open_door_rear"" < 0.5";
				statement="this animate [""Open_door_rear"", 1]";
			};
			class Close_door_rear: Open_door_rear
			{
				displayName="Close_door_rear";
				condition="this animationPhase ""Open_door_rear"" >= 0.5";
				statement="this animate [""Open_door_rear"", 0]";
			};
		};
		class CargoTurret;
		class Turrets: Turrets
		{
			class CargoTurret_01: CargoTurret
			{
				gunnerAction="passenger_inside_1";
				gunnerCompartments="Compartment1";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				gunnerName="Rear Right sit";
				proxyIndex=7;
				maxElev=45;
				minElev=-35;
				maxTurn=95;
				minTurn=-95;
				isPersonTurret=1;
				class dynamicViewLimits
				{
					CargoTurret_02[]={-65,95};
				};
			};
			class CargoTurret_02: CargoTurret_01
			{
				gunnerAction="passenger_inside_1";
				gunnerName="Rear Left sit";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				proxyIndex=8;
				class dynamicViewLimits
				{
					CargoTurret_01[]={-95,65};
				};
			};
			class MainTurret: MainTurret
			{
				body="mainTurret";
				gun="mainGun";
				memoryPointGun="usti hlavne";
				weapons[]=
				{
					"RGm19safe",
					"RGGMG_40mm"
				};
				magazines[]=
				{
					"96Rnd_40mm_G_belt",
					"96Rnd_40mm_G_belt",
					"96Rnd_40mm_G_belt",
					"96Rnd_40mm_G_belt"
				};
				memoryPointGunnerOptics="MainGunnerview";
				stabilizedInAxes=3;
				turretInfoType="RscOptics_Offroad_01";
				discreteDistance[]={100,200,300,400,500,600,700,800,900,1000,1100};
				discreteDistanceInitIndex=3;
				class GunFire: WeaponCloudsMGun
				{
					interval=0.0099999998;
				};
			};
		};
		class Damage
		{
			tex[]={};
			mat[]=
			{
				"EODS_RG31\data\auta_skla.rvmat",
				"EODS_RG31\data\exa_rg31_glass_full_d.rvmat",
				"EODS_RG31\data\exa_rg31_glass_full_d.rvmat",
				"EODS_RG31\data\exa_rg31_body.rvmat",
				"EODS_RG31\data\exa_rg31_body_damage.rvmat",
				"EODS_RG31\data\exa_rg31_body_destruct.rvmat",
				"EODS_RG31\data\exa_rg31_body2.rvmat",
				"EODS_RG31\data\exa_rg31_body2_damage.rvmat",
				"EODS_RG31\data\exa_rg31_body2_destruct.rvmat",
				"EODS_RG31\data\exa_RG31_interior.rvmat",
				"EODS_RG31\data\exa_RG31_interior.rvmat",
				"EODS_RG31\data\exa_RG31_interior_destruct.rvmat",
				"EODS_RG31\data\exa_RG31_interior2.rvmat",
				"EODS_RG31\data\exa_RG31_interior2.rvmat",
				"EODS_RG31\data\exa_RG31_interior2_destruct.rvmat"
			};
		};
	};
	class ExA_RG31_Mk19_OD: ExA_RG31_Mk19
	{
		scope=2;
		model="\EODS_RG31\ExA_RG31_mk19_OD";
		displayName="RG-31 Mk.19 OD (ExA)";
		class CargoTurret;
		class Turrets: Turrets
		{
			class CargoTurret_01: CargoTurret
			{
				gunnerAction="passenger_inside_1";
				gunnerCompartments="Compartment1";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				gunnerName="Rear Right sit";
				proxyIndex=7;
				maxElev=45;
				minElev=-35;
				maxTurn=95;
				minTurn=-95;
				isPersonTurret=1;
				class dynamicViewLimits
				{
					CargoTurret_02[]={-65,95};
				};
			};
			class CargoTurret_02: CargoTurret_01
			{
				gunnerAction="passenger_inside_1";
				gunnerName="Rear Left sit";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				proxyIndex=8;
				class dynamicViewLimits
				{
					CargoTurret_01[]={-95,65};
				};
			};
			class MainTurret: MainTurret
			{
				body="mainTurret";
				gun="mainGun";
				memoryPointGun="usti hlavne";
				weapons[]=
				{
					"RGm19safe",
					"RGGMG_40mm"
				};
				magazines[]=
				{
					"96Rnd_40mm_G_belt",
					"96Rnd_40mm_G_belt",
					"96Rnd_40mm_G_belt",
					"96Rnd_40mm_G_belt"
				};
				memoryPointGunnerOptics="MainGunnerview";
				stabilizedInAxes=3;
				turretInfoType="RscOptics_Offroad_01";
				discreteDistance[]={100,200,300,400,500,600,700,800,900,1000,1100};
				discreteDistanceInitIndex=3;
				class GunFire: WeaponCloudsMGun
				{
					interval=0.0099999998;
				};
			};
		};
	};
	class ExA_RG31E_M2: ExA_RG31_M2
	{
		scope=2;
		model="\EODS_RG31\ExA_RG31_MK5E_50";
		displayName="RG-31 Mk5E M2 (ExA)";
		transportSoldier=6;
		wheelCircumference=3.316;
		class CargoTurret;
		class Turrets: Turrets
		{
			class CargoTurret_01: CargoTurret
			{
				gunnerAction="passenger_inside_1";
				gunnerCompartments="Compartment1";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				gunnerName="Rear Right sit";
				proxyIndex=8;
				maxElev=45;
				minElev=-35;
				maxTurn=95;
				minTurn=-95;
				isPersonTurret=1;
				class dynamicViewLimits
				{
					CargoTurret_02[]={-65,95};
				};
			};
			class CargoTurret_02: CargoTurret_01
			{
				gunnerAction="passenger_inside_1";
				gunnerName="Rear Left sit";
				memoryPointsGetInGunner="pos cargo";
				memoryPointsGetInGunnerDir="pos cargo dir";
				proxyIndex=7;
				class dynamicViewLimits
				{
					CargoTurret_01[]={-95,65};
				};
			};
			class MainTurret: MainTurret
			{
				body="mainTurret";
				gun="mainGun";
				weapons[]=
				{
					"RGm2safe",
					"HMG_M2"
				};
				magazines[]=
				{
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red",
					"100Rnd_127x99_mag_Tracer_Red"
				};
				soundServo[]=
				{
					"\EODS_RG31\data\sound\turret",
					1,
					1,
					70
				};
				minElev=-15;
				maxElev=30;
				gunnerAction="rg31_Gunner";
				viewGunnerInExternal=1;
				castGunnerShadow=1;
				memoryPointGunnerOptics="MainGunnerview";
				stabilizedInAxes="StabilizedInAxesBoth";
				turretInfoType="RscOptics_Offroad_01";
				discreteDistance[]={100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500};
				discreteDistanceInitIndex=2;
				class ViewOptics;
				class ViewGunner: ViewOptics
				{
				};
			};
		};
		showNVGCargo[]={0,1};
		class AnimationSources: AnimationSources
		{
			class ReloadAnim
			{
				source="reload";
				weapon="HMG_M2";
			};
			class ReloadMagazine
			{
				source="reloadmagazine";
				weapon="HMG_M2";
			};
			class Revolving
			{
				source="revolving";
				weapon="HMG_M2";
			};
			class camonet
			{
				source="user";
				animPeriod=0.1;
				initPhase=0;
			};
			class belt_rotation
			{
				source="reload";
				weapon="GMG_40mm";
			};
			class BeaconsStart
			{
				source="user";
				animPeriod=1;
				initPhase=1;
			};
			class Open_door_rear
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
		};
		class UserActions
		{
			class beacons_start
			{
				userActionID=50;
				displayName="Beacons start";
				position="mph_axis";
				radius=1.8;
				onlyForplayer="false";
				condition="this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",0];";
			};
			class beacons_stop: beacons_start
			{
				userActionID=51;
				displayName="Beacons stop";
				condition="this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player";
				statement="this animate [""BeaconsStart"",1];";
			};
			class Open_door_rear
			{
				displayName="Open_door_rear";
				onlyforplayer="false";
				position="Door_rear_Trig";
				radius=2;
				condition="this animationPhase ""Open_door_rear"" < 0.5";
				statement="this animate [""Open_door_rear"", 1]";
			};
			class Close_door_rear: Open_door_rear
			{
				displayName="Close_door_rear";
				condition="this animationPhase ""Open_door_rear"" >= 0.5";
				statement="this animate [""Open_door_rear"", 0]";
			};
		};
	};
};
class cfgMods
{
	author="76561197982468872";
	timepacked="1441632016";
};
