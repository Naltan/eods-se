/*
init.sqf
Author(s): MrEwok , NorX Aengell
Description: Initialize Minedetector functions.
*/

waitUntil { sleep 0.5; !(isNull player) };
waitUntil { sleep 1.271; time > 0 };

EODS_MDET_var_active = false;
EODS_MDET_Uxo_var_active = false;

/// COMMON VARIABLES
if (isNil "EODS_MDET_var_radius") then { EODS_MDET_var_radius = 5 }; // Maximum distance from player to mine
if (isNil "EODS_MDET_var_angle") then { EODS_MDET_var_angle = 60 }; // Player to mine direction angle threshold

/// RANDOMIZE DISTANCE VALUE
if (isNil "EODS_MDET_var_rnd_radius") then { EODS_MDET_var_rnd_radius = true }; // Enable/Disable randomizer
if (isNil "EODS_MDET_var_rnd_radius_value") then { EODS_MDET_var_rnd_radius_value = 7 }; // +/- %

/// LOAD FUNCTIONS
call compile preprocessFileLineNumbers "EODS_Detector\Functions\functions.sqf";

player addEventHandler ["Respawn", {
	player execVM 'EODS_Detector\Functions\init.sqf';
}];


/*
_textoEstructurado_mdet_action_1 = "<img image='\EODS_ieds\data\logo_addaction.paa' size='1.5' shadow='false'/>" + " | " + localize "STR_EODS_MDET_TOGGLE_ON";
_textoEstructurado_mdet_action_2 = "<img image='\EODS_ieds\data\logo_addaction.paa' size='1.5' shadow='false'/>" + " | " + localize "STR_EODS_MDET_TOGGLE_OFF";
*/

[player, 1, ["ACE_SelfActions", "ACE_Equipment"], ["ActivateDetector", "Activate Mine Detector", "", {call EODS_MDET_fnc_switchState}, {alive _target && {currentWeapon _target in ["EODS_Detector_Weapon", "EODS_VMH3CS_Weapon"]} && !EODS_MDET_var_active}] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], ["DeactivateDetector", "Deactivate Mine Detector", "", {call EODS_MDET_fnc_switchState}, {alive _target && {currentWeapon _target in ["EODS_Detector_Weapon", "EODS_VMH3CS_Weapon"]} && EODS_MDET_var_active}] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

[player, 1, ["ACE_SelfActions", "ACE_Equipment"], ["ActivateUXODetector", "Activate UXO Detector", "", {call EODS_MDET_fnc_switchState_Uxo}, {alive _target && {currentWeapon _target in ["EODS_VMH3CS_UXO_head_Weapon"]} && !EODS_MDET_Uxo_var_active}] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], ["DeactivateUXODetector", "Deactivate UXO Detector", "", {call EODS_MDET_fnc_switchState_Uxo}, {alive _target && {currentWeapon _target in ["EODS_VMH3CS_UXO_head_Weapon"]} && EODS_MDET_Uxo_var_active}] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

while { alive player } do {
	//if (call EODS_MDET_fnc_canDetect) then {
	if (EODS_MDET_var_active) then {
			_nearestMine = call EODS_MDET_fnc_getNearestMine;
			if (isNil "_nearestMine") then {
				_nearestMine = 0;
			};
		//	_nearestIED = call EODS_MDET_fnc_getNearestIED;
			if (count _nearestMine > 0) then {
				_nearestMine call EODS_MDET_fnc_mineInRange;
			};
		/*	if (count _nearestIED > 0) then {
				_nearestIED call EODS_MDET_fnc_mineInRange;
			};*/
		//} else {
			// sleep 2.381;
		//};
	} else {
		if (EODS_MDET_var_active) then {
			[] call EODS_MDET_fnc_switchState
		};
	};
	sleep .503;
};

while { alive player } do {
		if (EODS_MDET_Uxo_var_active) then {
			_nearestUxo = call EODS_MDET_fnc_getNearestUxo;
			if (count _nearestUxo > 0) then {
				_nearestUxo call EODS_MDET_fnc_UxoInRange;
			};
	} else {
		if (EODS_MDET_Uxo_var_active) then {
			[] call EODS_MDET_fnc_switchState_Uxo
		};
	};
	sleep .503;
};

player removeAction _EODS_mdet_action_2;

player removeAction _EODS_mdet_action_3;
player removeAction _EODS_mdet_action_4;